#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <vector>
#include <queue>
#include <fstream>
#include "windows.h"


LARGE_INTEGER frequency;        // ticks per second
LARGE_INTEGER t1, t2, t3, t4;           // ticks
double elapsedTime;
int r_SIZE;
int C_max;
int U;
int UB=INT_MAX;

using namespace std;

struct rpq
{
    int r,p,q;
    int id;
};

struct compq
{
    bool operator()(const rpq &a, const rpq &b)
    {
        return a.q<b.q;
    }
};

struct compr
{
    bool operator() (const rpq &a, const rpq &b)
    {
        return a.r>b.r;
    }
};

void GenerateRpqVector(vector<rpq> &tasks,int V_SIZE)
{
    for(int i=0;i<V_SIZE;++i)
    {
        rpq x;
        x.r=rand()%1000+1;
        x.p=rand()%1000+1;
        x.q=rand()%1000+1;
        x.id=i;
        tasks.push_back(x);
    }

}

bool compare_id(rpq &a,rpq &b)
{
    return a.id<b.id;
}
void WyswietlPi(vector<rpq> tasks)
{
    for(int i=0;i<tasks.size();i++)
        cout << tasks[i].id<< " ";
        cout << endl;
}

void ReadRPQ(fstream& plik,vector<rpq>& tasks)
{
    int i=0;
    int useless;
    plik >> r_SIZE >> useless;
    while(i<r_SIZE)
    {
        rpq x;
        plik >> x.r >> x.p >> x.q;
        x.id=i;
        tasks.push_back(x);
        ++i;
    }
    plik.close();
}

int Schrage(vector<rpq> tasks,int kolejnosc[], int &b, int *C_task)
{
    int i=0, Cmax=0;
    priority_queue<rpq,vector<rpq>,compr> NN;
    priority_queue<rpq,vector<rpq>,compq> NG;
    rpq j;
    for(int i=0;i<tasks.size();++i)
    {
        NN.push(tasks[i]);
    }
    int t=NN.top().r;

    while(!NN.empty() || ! NG.empty())
    {
        while(!NN.empty() && NN.top().r<=t)
        {
            j=NN.top();
            NG.push(j);
            NN.pop();
        }
        if(NG.empty())
            t=NN.top().r;
        else
        {
            j=NG.top();
            NG.pop();
            t+=j.p;
			C_task[i] = t;
			if (Cmax <= t + j.q) {
				b = i;
				Cmax = t + j.q;
			}
			kolejnosc[i] = j.id;
			++i;
		}
	}
	return Cmax;
}

int Pschrage(vector<rpq> tasks)
{

	int t=0, cmax=0;
	priority_queue<rpq, vector<rpq>, compq> NG;
    priority_queue<rpq, vector<rpq>, compr> NN;
	rpq j, l;
	l.q = INT_MAX;
	for (int k=0;k<tasks.size();++k)
		NN.push(tasks[k]);
	while (!NN.empty() || !NG.empty())
	{
		while (!NN.empty() && NN.top().r <= t)
		{
			j = NN.top();
			NG.push(j);
			NN.pop();
			if (j.q > l.q)
			{
				l.p = t - j.r;
				t = j.r;

				if (l.p > 0)
					NG.push(l);
			}
		}
		if (NG.empty())
			t = NN.top().r;
		else
		{
			j = NG.top();
			NG.pop();
			l = j;
			t += j.p;
			cmax = max(cmax, t + j.q);
		}
	}
	return cmax;
}

void Carlier(vector<rpq> tasks) {

	int *KOLEJNOSC = new int[r_SIZE];
	int b;
    int *kolejnosc = new int[r_SIZE];
    int a, c = -1;
    int temp, temp_q;
    int *C_task = new int[r_SIZE];
    U = Schrage(tasks,kolejnosc,b,C_task);
    int LB;

    if (U < UB) {
        UB = U;
        KOLEJNOSC = kolejnosc;
    }

    int k;
    temp_q = tasks[kolejnosc[b]].q;
    a = b;
    while ((a >= 1) && (C_task[a - 1] >= tasks[a].r)) {
        a--;
    }

    for (int i = b; i >= a; --i) {
		k = kolejnosc[i];
        temp = tasks[k].q;
        if (temp < temp_q) {
            c = i;
            break;
        }
    }
    if (c == -1)
        return;

    int r_ = tasks[kolejnosc[c + 1]].r;
    int q_ = tasks[kolejnosc[c + 1]].q;
    int p_ = 0;
    for (int i = c + 1; i <= b; ++i) {
        k = kolejnosc[i];
        r_ = min(r_, tasks[k].r);
        q_ = min(q_, tasks[k].q);
        p_ += tasks[k].p;
    }

    int c_iterator = kolejnosc[c];

    if (tasks[c_iterator].r < r_ + p_) {
        temp = tasks[c_iterator].r;
        tasks[c_iterator].r = r_ + p_;
        LB = Pschrage(tasks);
        if (LB < UB)
            Carlier(tasks);
        tasks[c_iterator].r = temp;
    }

    if (tasks[c_iterator].q < q_ + p_) {
        temp = tasks[c_iterator].q;
        tasks[c_iterator].q = q_ + p_;
        LB = Pschrage(tasks);
        if (LB < UB)
            Carlier(tasks);
        tasks[c_iterator].q = temp;
    }
}

int main()
{
    srand (time(NULL));
    vector<rpq> tasks;

    fstream plik;
    plik.open("in200.txt",ios::in);
    ReadRPQ(plik,tasks);
    Carlier(tasks);
    cout << "Carlier: " << U;

    return 0;
}
