#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <vector>
#include <queue>
#include <fstream>
#include "windows.h"

LARGE_INTEGER frequency;        // ticks per second
LARGE_INTEGER t1, t2, t3, t4;           // ticks
double elapsedTime;
int r_SIZE=50;
int C_max;

using namespace std;

struct rpq
{
    int r,p,q;
    int id;
};

bool compare_r(rpq &a,rpq &b)
{
    return a.r<b.r;
}

bool compare_q(rpq &a,rpq &b)
{
    return a.q<b.q;
}

struct compq
{
    bool operator()(const rpq &a, const rpq &b)
    {
        return a.q<b.q;
    }
};

struct compr
{
    bool operator() (const rpq &a, const rpq &b)
    {
        return a.r>b.r;
    }
};

void GenerateRpqVector(vector<rpq> &tasks,int V_SIZE)
{
    for(int i=0;i<V_SIZE;++i)
    {
        rpq x;
        x.r=rand()%1000+1;
        x.p=rand()%1000+1;
        x.q=rand()%1000+1;
        x.id=i;
        tasks.push_back(x);
    }

}

bool compare_id(rpq &a,rpq &b)
{
    return a.id<b.id;
}
void WyswietlPi(vector<rpq> tasks)
{
    for(int i=0;i<tasks.size();i++)
        cout << tasks[i].id<< " ";
        cout << endl;
}

int Cpi(vector<rpq> &tasks, int j)
{
    if(j==0) return max(tasks[j].r,0)+tasks[j].p; // obej��ie ujemnego indeksu wektora
    else return max(tasks[j].r,Cpi(tasks,j-1))+tasks[j].p;
}

int Cmax(vector<rpq> &tasks)
{
    int temp,maks=0;
    for(int i=0;i<tasks.size();i++)
    {
        temp=(Cpi(tasks,i)+tasks[i].q);
        if(temp>maks)
            maks=temp;
    }
    return maks;
}

void Schrage(vector<rpq> tasks, vector<rpq>& rozw)
{
    int i=0;
    priority_queue<rpq,vector<rpq>,compr> NN;
    priority_queue<rpq,vector<rpq>,compq> NG;
    rpq j;
    for(int j=0;j<tasks.size();++j)
    {
        NN.push(tasks[j]);
    }
    int t=NN.top().r;

    while(!NN.empty() || ! NG.empty())
    {
        while(!NN.empty() && NN.top().r<=t)
        {
            j=NN.top();
            NG.push(j);
            NN.pop();
        }
        if(NG.empty())
            t=NN.top().r;
        else
        {
            j=NG.top();
            NG.pop();
            rozw[i]=j;
            t+=j.p;
            ++i;
        }
    }
     C_max=Cmax(rozw);
}

void Schrage_n2(vector<rpq> tasks, vector<rpq>& rozw)
{
    int i=0;
    vector<rpq> NN;
    vector<rpq> NG;
    rpq j;
    for(int k=0;k<tasks.size();++k)
    {
        NN.push_back(tasks[k]);
    }
    int t=(*min_element(NN.begin(),NN.end(),&compare_r)).r;

    while(!NN.empty() || ! NG.empty())
    {
        while(!NN.empty() && (*min_element(NN.begin(),NN.end(),&compare_r)).r<=t)
        {
            j=(*min_element(NN.begin(),NN.end(),&compare_r));
            NG.push_back(j);
            NN.erase(min_element(NN.begin(),NN.end(),&compare_r));
        }
        if(NG.empty())
            t=(*min_element(NN.begin(),NN.end(),&compare_r)).r;
        else
        {
            j=(*max_element(NG.begin(),NG.end(),&compare_q));
            NG.erase(max_element(NG.begin(),NG.end(),&compare_q));
            rozw[i]=j;
            t+=j.p;
            ++i;
        }
    }
    C_max=Cmax(rozw);
}

void Pschrage(vector<rpq> tasks, vector<rpq>& rozw)
{

	int t=0, cmax=0;
	priority_queue<rpq, vector<rpq>, compq> NG;
    priority_queue<rpq, vector<rpq>, compr> NN;
	rpq j, l;
	l.q = INT_MAX;
	for (int k=0;k<tasks.size();++k)
		NN.push(tasks[k]);
	while (!NN.empty() || !NG.empty())
	{
		while (!NN.empty() && NN.top().r <= t)
		{
			j = NN.top();
			NG.push(j);
			NN.pop();
			if (j.q > l.q)
			{
				l.p = t - j.r;
				t = j.r;

				if (l.p > 0)
					NG.push(l);
			}
		}
		if (NG.empty())
			t = NN.top().r;
		else
		{
			j = NG.top();
			NG.pop();
			l = j;
			t += j.p;
			cmax = max(cmax, t + j.q);
		}
	}
	C_max=cmax;
}

int SortR(vector<rpq> tasks)
{
    sort(tasks.begin(),tasks.end(), &compare_r);
    return Cmax(tasks);
}

void ReadRPQ(fstream& plik,vector<rpq>& tasks)
{
    int i=0;
    while(i<r_SIZE)
    {
        rpq x;
        plik >> x.r >> x.p >> x.q;
        x.id=i;
        tasks.push_back(x);
        ++i;
    }
    plik.close();
}

void TestingS(int ileprob, int V_SIZE)
{
    double avg_time=0;
    for(int i=0;i<ileprob;++i)
    {
        vector<rpq> tasks;
        vector<rpq> rozw(V_SIZE);
        GenerateRpqVector(tasks,V_SIZE);
        QueryPerformanceCounter(&t1);
        Schrage(tasks,rozw);
        QueryPerformanceCounter(&t2);
        elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
        avg_time+=elapsedTime/ileprob;
    }
    cout << V_SIZE << " " << avg_time << endl;
}

int main()
{

    srand (time(NULL));
    vector<rpq> tasks;
    vector<rpq> rozw(r_SIZE);

    fstream plik;
    plik.open("in50.txt",ios::in);
    ReadRPQ(plik,tasks);
    cout<<"schrage PMTN"<<endl;
    Pschrage(tasks,rozw);
    cout<<C_max<<endl;
    cout<<"schrage"<<endl;
    Schrage(tasks,rozw);
    cout<<C_max<<endl;
    cout<<"schrage n2"<<endl;
    Schrage_n2(tasks,rozw);
    cout<<C_max<<endl;



    return 0;
}
