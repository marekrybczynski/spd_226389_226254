#include <iostream>
#include <stdlib.h>
#include <vector>
#include <list>
#include <deque>
#include <algorithm>
#include <ctime>
#include <windows.h>
#include <fstream>

#define ilemaszyn 5
#define ilezadan 10

using namespace std;

struct zadanie
{
    int maszyny[ilemaszyn];
    int czas;
    int id;
};

void GenerateTaskVector(deque<zadanie> &tasks)
{
    int czas=0;
   for(int i=0;i<ilezadan;++i)
    {
        tasks[i].id=i;
        for(int j=0;j<ilemaszyn;++j)
        {
            tasks[i].maszyny[j]=rand()%10+1;
            czas+=tasks[i].maszyny[j];
        }
        tasks[i].czas=czas;
        czas=0;
    }
    }




bool compare_czas(zadanie &a, zadanie &b)
{
    return a.czas>b.czas;
}

bool compare_id(zadanie &a, zadanie &b)
{
    return a.id<b.id;
}

int C_pi(deque<zadanie> &tasks,int i,int j)
{

    if(i==0 && j==0) return tasks[j].maszyny[i]; else
    if(i==0) return C_pi(tasks,i,j-1)+tasks[j].maszyny[i];
    else if(j==0) return C_pi(tasks,i-1,j)+tasks[j].maszyny[i];
    else return max(C_pi(tasks,i,j-1),C_pi(tasks,i-1,j))+tasks[j].maszyny[i];
}

int Cmax(deque<zadanie> &tasks,int ilem, int ilez)
{
    return C_pi(tasks,ilem-1,ilez-1);
}

void WyswietlPi(deque<zadanie> tasks)
{
    for(int i=0;i<tasks.size();++i)
        cout << tasks[i].id<< " ";
        cout << endl;
}

int NEH(deque<zadanie>&neh_tasks,deque<zadanie>& rozw)
{
   // deque<zadanie> tasks=neh_tasks;
    int C_max;
    int C_max_min=INT_MAX;
    int C_max_final;
    int pom_size=neh_tasks.size();
    deque<zadanie> temp;
    deque<zadanie> temp2;
    sort(neh_tasks.begin(),neh_tasks.end(), &compare_czas);
    zadanie x=*(neh_tasks.begin());
    temp.push_back(x);
    neh_tasks.pop_front();
    for(int i=0;i<pom_size-1;++i)
    {
        deque<zadanie>::iterator it=temp.begin();
        zadanie tmp=*(neh_tasks.begin());
        neh_tasks.pop_front();

        if(i!=neh_tasks.size()-1)
        C_max_min=INT_MAX;

        for(int j=0;j<temp.size()+1;++j)
        {
            it=temp.insert(it,tmp);
            //WyswietlPi(temp);
            C_max=Cmax(temp,ilemaszyn,temp.size());
            if(C_max<=C_max_min)
            {
                C_max_min=C_max;
                temp2=temp;
            }
            it=temp.erase(it);
            //WyswietlPi(temp);
            it++;
        }
        temp=temp2;
        //cout << neh_tasks.size()-1 << endl;
    }
    rozw=temp;
    //WyswietlPi(rozw);
    return C_max_min;
}

void WczytajNEH(fstream &plik, deque<zadanie>& tasks)
{
    int l=0;
    int czas=0;
    while(!plik.eof())
    {
        tasks[l].id=l;
        for(int i=0;i<ilemaszyn;++i)
        {
            plik >> tasks[l].maszyny[i];
            czas+=tasks[l].maszyny[i];
        }
        tasks[l].czas=czas;
        czas=0;
        l++;
    }

}
int main()
{
    srand (time(NULL));
    deque<zadanie> tasks(ilezadan);
    deque<zadanie> rozw(ilezadan);
    fstream plik;
    plik.open("NEH2.DAT");
   if(!plik.good()) cout<<"nein";

    WczytajNEH(plik,tasks);

cout << NEH(tasks,rozw);/*
  LARGE_INTEGER frequency;        // ticks per second
    LARGE_INTEGER t1, t2;
    srand(time(NULL));
    double elapsedTime;
    QueryPerformanceFrequency(&frequency);

    QueryPerformanceCounter(&t1);
    int ilosc=1000;
for(int i=0;i<ilosc;++i){
        GenerateTaskVector(tasks);
        NEH(tasks,rozw);}
    QueryPerformanceCounter(&t2);
    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    cout<<elapsedTime/ilosc<<endl;

//WyswietlPi(tasks);*/
}

