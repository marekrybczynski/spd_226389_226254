#include <iostream>
#include <stdlib.h>
#include <vector>
#include <list>
#include <algorithm>
#include <time.h>

#define ilemaszyn 3
#define ilezadan 4

using namespace std;

struct zadanie_3m
{
    int maszyny[ilemaszyn];
    int id;
};
struct zadanie // zadanie wirtualne
{
    int maszyny[ilemaszyn-1];
    int id;
};

void GenerateTaskVector(vector<zadanie_3m> &tasks)
{
    for(int i=0;i<ilezadan;++i)
    {
        zadanie_3m x;
        x.maszyny[0]=rand()%10+1;
        x.maszyny[1]=rand()%10+1;
        x.maszyny[2]=rand()%10+1;
        x.id=i;
        tasks[i]=x;
    }
}

void convert_tasks(vector<zadanie_3m> &tasks_3m, vector<zadanie> &tasks)
{
    for(int i=0;i<ilezadan;++i)
    {
        tasks[i].maszyny[0]=tasks_3m[i].maszyny[0]+tasks_3m[i].maszyny[1];
        tasks[i].maszyny[1]=tasks_3m[i].maszyny[1]+tasks_3m[i].maszyny[2];
        tasks[i].id=tasks_3m[i].id;
    }
}
struct zadanie_idmaszyny
{
    int czas;
    int id_m;
    int id;
};

bool compare_czas(zadanie_idmaszyny &a, zadanie_idmaszyny &b)
{
    return a.czas<b.czas;
}

bool compare_id(zadanie_3m &a, zadanie_3m &b)
{
    return a.id<b.id;
}

void tasks_johnson_2(list<zadanie_idmaszyny> &min_tasks, vector<zadanie> &tasks)
{
    for(int i=0;i<ilezadan;++i)
        {
            zadanie_idmaszyny x;
            x.czas=*(min_element(begin(tasks[i].maszyny),end(tasks[i].maszyny)));
            x.id_m=distance(begin(tasks[i].maszyny),min_element(begin(tasks[i].maszyny),end(tasks[i].maszyny)));
            x.id=tasks[i].id;
            min_tasks.push_back(x);
        }
}

void const WyswietlPi(vector<zadanie> tasks)
{
    for(int i=0;i<ilezadan;++i)
        cout << tasks[i].id<< " ";
        cout << endl;
}

int C_pi(vector<zadanie_3m> &tasks,int i,int j)
{

    if(i==0 && j==0) return tasks[j].maszyny[i]; else
    if(i==0) return C_pi(tasks,i,j-1)+tasks[j].maszyny[i];
    else if(j==0) return C_pi(tasks,i-1,j)+tasks[j].maszyny[i];
    else return max(C_pi(tasks,i,j-1),C_pi(tasks,i-1,j))+tasks[j].maszyny[i];
}

int Cmax(vector<zadanie_3m> &tasks)
{
    return C_pi(tasks,ilemaszyn-1,ilezadan-1);
}

vector<zadanie> Johnson_2m(vector<zadanie> &tasks, list<zadanie_idmaszyny> &min_tasks)
{
    int p=0;
    int k=ilezadan-1; // liczniki element�w na ko�cu i pocz�tku listy rozwi�zania
    vector<zadanie> rozw(ilezadan);
    for(int i=0;i<ilezadan;++i)
    {
        auto min_val=min_element(min_tasks.begin(),min_tasks.end(),&compare_czas);
        if((*min_val).id_m==0)
        {
            rozw[p]=tasks[(*min_val).id];
            ++p;
        }
        else
        {
            rozw[k]=tasks[(*min_val).id];
            --k;
        }
        min_tasks.erase(min_val);

    }
    return rozw;
}

int PrzegladZupelny(vector<zadanie_3m> &tasks)
{
    vector<zadanie_3m> rozw(ilezadan);
    int minimum=Cmax(tasks);
    do{
        if(Cmax(tasks)<minimum)
        {
            minimum=Cmax(tasks);
            rozw=tasks;
        }
        }
    while(next_permutation(begin(tasks),end(tasks),&compare_id));
    return minimum;
}

int main()
{
    srand (time(NULL));

    vector<zadanie_3m> tasks_3m(ilezadan);
    vector<zadanie> tasks(ilezadan);
    list<zadanie_idmaszyny> min_tasks;
    vector<zadanie> rozw(ilezadan);

    GenerateTaskVector(tasks_3m);
    convert_tasks(tasks_3m,tasks);
    tasks_johnson_2(min_tasks,tasks);
    WyswietlPi(Johnson_2m(tasks,min_tasks));
    //cout << PrzegladZupelny(tasks_3m);

}
