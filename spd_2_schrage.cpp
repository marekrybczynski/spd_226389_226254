#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <vector>
#include <queue>
#include <fstream>
#define SIZE 24

using namespace std;

struct rpq
{
    int r,p,q;
    int id;
};

struct compq
{
    bool operator()(const rpq &a, const rpq &b)
    {
        return a.q<b.q;
    }
};

struct compr
{
    bool operator() (const rpq &a, const rpq &b)
    {
        return a.r>b.r;
    }
};
void GenerateRpqVector(vector<rpq> &tasks)
{
    for(int i=0;i<SIZE;++i)
    {
        rpq x;
        x.r=rand()%10+1;
        x.p=rand()%10+1;
        x.q=rand()%10+1;
        x.id=i;
        tasks.push_back(x);
    }

}

bool compare_r(rpq &a,rpq &b)
{
    return a.r<b.r;
}

bool compare_id(rpq &a,rpq &b)
{
    return a.id<b.id;
}
void WyswietlPi(vector<rpq> tasks)
{
    for(int i=0;i<5;i++)
        cout << tasks[i].id<< " ";
        cout << endl;
}

int Cpi(vector<rpq> &tasks, int j)
{
    if(j==0) return max(tasks[j].r,0)+tasks[j].p; // obej��ie ujemnego indeksu wektora
    else return max(tasks[j].r,Cpi(tasks,j-1))+tasks[j].p;
}

int Cmax(vector<rpq> &tasks)
{
    int temp,maks=0;
    for(int i=0;i<tasks.size();i++)
    {
        temp=(Cpi(tasks,i)+tasks[i].q);
        if(temp>maks)
            maks=temp;
    }
    return maks;
}

void Schrage(vector<rpq> tasks, vector<rpq>& rozw)
{
    int i=0;
    priority_queue<rpq,vector<rpq>,compr> NN;
    priority_queue<rpq,vector<rpq>,compq> NG;
    rpq j;
    for(int i=0;i<tasks.size();++i)
    {
        NN.push(tasks[i]);
    }
    int t=NN.top().r;

    while(!NN.empty() || ! NG.empty())
    {
        while(!NN.empty() && NN.top().r<=t)
        {
            j=NN.top();
            NG.push(j);
            NN.pop();
        }
        if(NG.empty())
            t=NN.top().r;
        else
        {
            j=NG.top();
            NG.pop();
            rozw[i]=j;
            cout << rozw[i].id << endl;
            t+=j.p;
            ++i;
        }
    }
    cout << Cmax(rozw) << endl;
}

void ReadRPQ(fstream& plik,vector<rpq>& tasks)
{
    int i=0;
    while(plik)
    {
        cout << "help";
        rpq x;
        plik >> x.r >> x.p >> x.q;
        x.id=i;
        tasks.push_back(x);
        ++i;
    }
    plik.close();
}

int main()
{
    srand (time(NULL));
    vector<rpq> tasks;
    vector<rpq> rozw(SIZE);

    fstream plik;
    plik.open("dane.txt",ios::in);
    ReadRPQ(plik,tasks);
    //GenerateRpqVector(tasks);
    Schrage(tasks,rozw);
    WyswietlPi(rozw);
}
