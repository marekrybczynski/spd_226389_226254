#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <vector>
#define SIZE 5

using namespace std;

struct rpq
{
    int r,p,q;
    int id;
};

void GenerateRpqVector(vector<rpq> &tasks)
{
    for(int i=0;i<SIZE;++i)
    {
        rpq x;
        x.r=rand()%10+1;
        x.p=rand()%10+1;
        x.q=rand()%10+1;
        x.id=i;
        tasks[i]=x;
    }

}

bool compare_r(rpq &a,rpq &b)
{
    return a.r<b.r;
}

bool compare_id(rpq &a,rpq &b)
{
    return a.id<b.id;
}
void WyswietlPi(vector<rpq> &tasks)
{
    for(int i=0;i<SIZE;i++)
        cout << tasks[i].id<< " ";
        cout << endl;
}

int Cpi(vector<rpq> &tasks, int j)
{
    if(j==0) return tasks[j].r+tasks[j].p; // obej��ie ujemnego indeksu wektora
    else return max(tasks[j].r,Cpi(tasks,j-1))+tasks[j].p;
}

int Cmax(vector<rpq> &tasks)
{
    int temp,maks=0;
    for(int i=0;i<SIZE;i++)
    {
        temp=(Cpi(tasks,i)+tasks[i].q);
        if(temp>maks)
            maks=temp;
    }
    return maks;
}

int PrzegladZupelny(vector<rpq> &tasks)
{
    int minimum=Cmax(tasks);
    do{
        if(Cmax(tasks)<minimum)
        {
            minimum=Cmax(tasks);
        }
        }
    while(next_permutation(tasks.begin(),tasks.end(),&compare_id));
    return minimum;
}

int SortR(vector<rpq> &tasks)
{
    sort(tasks.begin(),tasks.end(), &compare_r);
    return Cmax(tasks);
}

int main()
{
    srand (time(NULL));
    vector<rpq> tasks(SIZE);
    GenerateRpqVector(tasks);
    cout << PrzegladZupelny(tasks) << endl;
    cout << SortR(tasks) << endl;
}
