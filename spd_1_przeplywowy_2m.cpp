#include <iostream>
#include <stdlib.h>
#include <vector>
#include <list>
#include <algorithm>
#include <time.h>

#define ilemaszyn 2
#define ilezadan 6

using namespace std;

struct zadanie
{
    int maszyny[ilemaszyn];
    int id;
};

struct zadanie_idmaszyny
{
    int czas;
    int id_m;
    int id;
};

void GenerateTaskVector(vector<zadanie> &tasks)
{
    for(int i=0;i<ilezadan;++i)
    {
        zadanie x;
        x.maszyny[0]=rand()%10+1;
        x.maszyny[1]=rand()%10+1;
        x.id=i;
        tasks[i]=x;
    }
}

bool compare_czas(zadanie_idmaszyny &a, zadanie_idmaszyny &b)
{
    return a.czas<b.czas;
}

bool compare_id(zadanie &a, zadanie &b)
{
    return a.id<b.id;
}

void tasks_johnson_2(list<zadanie_idmaszyny> &min_tasks, vector<zadanie> &tasks)
{
    for(int i=0;i<ilezadan;++i)
        {
            zadanie_idmaszyny x;
            x.czas=*(min_element(begin(tasks[i].maszyny),end(tasks[i].maszyny)));
            x.id_m=distance(begin(tasks[i].maszyny),min_element(begin(tasks[i].maszyny),end(tasks[i].maszyny)));
            x.id=tasks[i].id;
            min_tasks.push_back(x);
        }
}

void WyswietlPi(vector<zadanie> &tasks)
{
    for(int i=0;i<ilezadan;++i)
        cout << tasks[i].id<< " ";
        cout << endl;
}

int C_pi(vector<zadanie> &tasks,int i,int j)
{

    if(i==0 && j==0) return tasks[j].maszyny[i]; else
    if(i==0) return C_pi(tasks,i,j-1)+tasks[j].maszyny[i];
    else if(j==0) return C_pi(tasks,i-1,j)+tasks[j].maszyny[i];
    else return max(C_pi(tasks,i,j-1),C_pi(tasks,i-1,j))+tasks[j].maszyny[i];
}

int Cmax(vector<zadanie> &tasks)
{
    return C_pi(tasks,ilemaszyn-1,ilezadan-1);
}

int Johnson_2m(vector<zadanie> &tasks, list<zadanie_idmaszyny> &min_tasks)
{
    int p=0;
    int k=ilezadan-1; // liczniki elementow na koncu i poczatku wektora rozwiazania
    vector<zadanie> rozw(ilezadan);
    for(int i=0;i<ilezadan;++i)
    {
        auto min_val=min_element(min_tasks.begin(),min_tasks.end(),&compare_czas);
        if((*min_val).id_m==0)
        {
            rozw[p]=tasks[(*min_val).id];
            ++p;
        }
        else
        {
            rozw[k]=tasks[(*min_val).id];
            --k;
        }
        min_tasks.erase(min_val);

    }
    WyswietlPi(rozw);
    return Cmax(rozw);
}

int PrzegladZupelny(vector<zadanie> &tasks)
{
    vector<zadanie> rozw(ilezadan);
    int minimum=Cmax(tasks);
    do{
        if(Cmax(tasks)<minimum)
        {
            minimum=Cmax(tasks);
            rozw=tasks;
        }
        }
    while(next_permutation(begin(tasks),end(tasks),&compare_id));
    return minimum;
}

int main()
{
    srand (time(NULL));
    vector<zadanie> tasks(ilezadan);
    list<zadanie_idmaszyny> min_tasks;
    vector<zadanie> rozw(ilezadan);

    GenerateTaskVector(tasks);
    tasks_johnson_2(min_tasks,tasks);
    Johnson_2m(tasks,min_tasks);
}
