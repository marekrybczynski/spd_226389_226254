#include <iostream>
#include <stdlib.h>
#include <vector>
#include <list>
#include <algorithm>
#include <ctime>
#include <fstream>
#include <string>
#include "windows.h"

#define ilemaszyn 3
#define ilezadan 50

using namespace std;


LARGE_INTEGER frequency;
LARGE_INTEGER t1, t2, t3, t4;
double elapsedTime;

struct zadanie
{
    int maszyny[ilemaszyn];
    int czas;
    int id;
};

void GenerateTaskVector(vector<zadanie> &tasks)
{
    int czas=0;
    for(int i=0;i<ilezadan;++i)
    {
        zadanie x;
        for(int j=0;j<ilemaszyn;++j)
        {
            x.maszyny[j]=rand()%50+1;
            czas+=tasks[i].maszyny[j];
        }
        x.id=i;
        tasks[i]=x;
        x.czas=czas;
        czas=0;
    }
}

bool compare_czas(zadanie &a, zadanie &b)
{
    return a.czas>b.czas;
}

bool compare_id(zadanie &a, zadanie &b)
{
    return a.id<b.id;
}

bool operator == (const vector<zadanie> &a, const vector<zadanie> &b)
{
    bool val=true;
    for(int i=0;i<a.size();++i)
    {
        if(a[i].id!=b[i].id)
            val=false;
    }
    return val;
}

int C_pi(vector<zadanie> &tasks,int i,int j)
{

    if(i==0 && j==0) return tasks[j].maszyny[i]; else
    if(i==0) return C_pi(tasks,i,j-1)+tasks[j].maszyny[i];
    else if(j==0) return C_pi(tasks,i-1,j)+tasks[j].maszyny[i];
    else return max(C_pi(tasks,i,j-1),C_pi(tasks,i-1,j))+tasks[j].maszyny[i];
}

int Cmax(vector<zadanie> &tasks,int ilem, int ilez)
{
    return C_pi(tasks,ilem-1,ilez-1);
}

void WyswietlPi(vector<zadanie> tasks)
{
    for(int i=0;i<tasks.size();++i)
        cout << tasks[i].id<< " ";
        cout << endl;
}

vector<vector<zadanie>> getSwaps(vector<zadanie> tasks)
{
    vector<zadanie> tmp;
    tmp=tasks;
    vector<vector<zadanie>> neighbors;
    for(int i=0;i<tasks.size()-1;++i)
    {
        int j=i+1;
        for(j;j<tasks.size();++j)
        {
            swap(tmp[i],tmp[j]);
            neighbors.push_back(tmp);
            tmp=tasks;
        }
    }
        return neighbors;
}

vector<vector<zadanie>> getInserts(vector<zadanie> tasks)
{
    vector<zadanie> tmp;
    tmp=tasks;
    vector<vector<zadanie>> neighbors;
    for(int i=0;i<tasks.size();++i)
    {
        zadanie x=tasks[i];
        tmp.erase(tmp.begin()+i);
        for(int j=0;j<tasks.size();++j)
        {
            tmp.insert(tmp.begin()+j,x);
            neighbors.push_back(tmp);
            tmp.erase(tmp.begin()+j);
        }
        tmp=tasks;
    }
    return neighbors;
}


int TS(vector<zadanie>tasks, vector<zadanie> &rozw,int rozmiar_tabu,int swap_insert,double ile_pr_sasiadow)
{
    int maxTabuSize=rozmiar_tabu;
    int ile_iter=0;
    int C_max_min=INT_MAX;
    int rand_num;
    int previous_C_max=INT_MAX;
    rozw = tasks;
    vector<zadanie> bestCandidate = tasks;
    list<vector<zadanie>> tabuList;
    vector<vector<zadanie>> neighborList;
    vector<zadanie> tmp;
    tabuList.push_back(tasks);


    while(1)
    {
        if(swap_insert == 1)
        neighborList=getSwaps(bestCandidate);
        if(swap_insert == 2)
        neighborList=getInserts(bestCandidate);
        bestCandidate=neighborList[0];
        if(ile_pr_sasiadow==1)
        for(int i=0;i<neighborList.size();++i)
        {
            //WyswietlPi(neighborList[i]);
            //cout << Cmax(neighborList[i],ilemaszyn,ilezadan) << endl;
            if(Cmax(neighborList[i],ilemaszyn,ilezadan)<Cmax(bestCandidate,ilemaszyn,ilezadan))
            {
                if(find(tabuList.begin(),tabuList.end(),neighborList[i])==tabuList.end())
                    bestCandidate=neighborList[i];
            }
        }
        if(ile_pr_sasiadow<1)
        for(int i=0;i<(ile_pr_sasiadow*neighborList.size());++i)
        {
            //WyswietlPi(neighborList[i]);
            //cout << Cmax(neighborList[i],ilemaszyn,ilezadan) << endl;
            rand_num=rand() %neighborList.size();
            //cout << rand_num << endl;
            if(Cmax(neighborList[rand_num],ilemaszyn,ilezadan)<Cmax(bestCandidate,ilemaszyn,ilezadan))
            {
                if(find(tabuList.begin(),tabuList.end(),neighborList[rand_num])==tabuList.end())
                    bestCandidate=neighborList[rand_num];
            }
        }
        //cout << endl << endl;
        C_max_min=Cmax(bestCandidate,ilemaszyn,ilezadan);
        //cout << ile_iter << "  " << C_max_min << endl;
        if(C_max_min < Cmax(rozw,ilemaszyn,ilezadan))
            rozw = bestCandidate;
        else break;
        tabuList.push_back(bestCandidate);
        if(tabuList.size()>maxTabuSize)
            tabuList.pop_front();
        ++ile_iter;

    }
    return Cmax(rozw,ilemaszyn,ilezadan);

}

void WczytajNEH(fstream& plik, vector<zadanie>& tasks)
{
    int l=0;
    int czas=0;
    while(!plik.eof())
    {
        tasks[l].id=l;
        for(int i=0;i<ilemaszyn;++i)
        {
            plik >> tasks[l].maszyny[i];
            czas+=tasks[l].maszyny[i];
        }
        tasks[l].czas=czas;
        czas=0;
        l++;
    }

}
void TestowanieTS(vector<zadanie> tasks_ts,vector<zadanie> rozw,int rozmiar_tabu,int swap_insert,double ile_pr_sasiadow)
{
    elapsedTime=0;
    for(int i=0;i<5;++i)
    {
        GenerateTaskVector(tasks_ts);
        QueryPerformanceCounter(&t1);
        TS(tasks_ts,rozw,rozmiar_tabu,swap_insert,ile_pr_sasiadow);
        QueryPerformanceCounter(&t2);
        elapsedTime += (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    }
    cout << elapsedTime/5 << endl;
};


int main()
{
    srand (time(NULL));
    QueryPerformanceFrequency(&frequency);
    vector<zadanie> tasks_ts(ilezadan);
    vector<zadanie> rozw_ts(ilezadan);
    fstream plik;
    plik.open("dane1.txt");
    if(!plik.good()) cout << "nie dziala" << endl;
    WczytajNEH(plik,tasks_ts);
    //GenerateTaskVector(tasks_ts);

    cout << TS(tasks_ts,rozw_ts,10000,1,1) << endl;
    cout << TS(tasks_ts,rozw_ts,10000,2,1) << endl;

  return 0;
}

