#include <iostream>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <list>

#define ilemaszyn 2
#define ilezadan 6

using namespace std;

struct zadanie
{
    int maszyny[ilemaszyn];
    int id;
};

struct zadanie_1m
{
    int czas;
    int id_masz;
};

bool compare_id(zadanie &a, zadanie &b)
{
    return a.id<b.id;
}

bool compare_czas(zadanie_1m &a, zadanie_1m &b)
{
    return a.czas<b.czas;
}


void WyswietlPi(vector<zadanie> &tasks)
{
    for(int i=0;i<ilezadan;++i)
        cout << tasks[i].id<< " ";
        cout << endl;
}

void AllTasksList(list<zadanie_1m> &t_list,vector<zadanie> & tasks)
{
    for(int i=0;i<ilezadan;++i)
        for(int j=0;j<ilemaszyn;++j)
    {
        t_list.push_back();
    }
}

int C_pi(vector<zadanie> &tasks,int i,int j)
{

    if(i==0 && j==0) return tasks[j].maszyny[i]; else
    if(i==0) return C_pi(tasks,i,j-1)+tasks[j].maszyny[i];
    else if(j==0) return C_pi(tasks,i-1,j)+tasks[j].maszyny[i];
    else return max(C_pi(tasks,i,j-1),C_pi(tasks,i-1,j))+tasks[j].maszyny[i];
}

int Cmax(vector<zadanie> &tasks)
{
    return C_pi(tasks,ilemaszyn-1,ilezadan-1);
}

int PrzegladZupelny(vector<zadanie> &rozw, vector<zadanie> &tasks)
{
    int minimum=Cmax(tasks);
    do{
        if(Cmax(tasks)<minimum)
        {
            minimum=Cmax(tasks);
            rozw=tasks;
        }
        }
    while(next_permutation(begin(tasks),end(tasks),&compare_id));
    return minimum;
}

int Johnson_2maszyny(vector<zadanie> &rozw, list<zadanie_1m> &tasks)
{
    int ile1=0,ile2=0;
    for(int i=0;i<tasks.size();++i)
    {
        if(min_element(tasks.begin(),tasks.end(),compare_czas).id_masz==0)
        {
            rozw[ile1].maszyny[0]= tasks.min_element(tasks.begin(),tasks.end(),compare_czas);
            ++ile1;
        }
        else rozw[ilezadan-1-ile2].maszyny[1]=min_element(tasks.begin(),tasks.end(),compare_czas);
            ++ ile2;
            }
}

int main()
{
    vector<zadanie> tasks(ilezadan);
    vector<zadanie> rozw(ilezadan);
    list<zadanie> t_list;

    tasks[0].maszyny[0]=5;
    tasks[0].maszyny[1]=6;
    tasks[0].id=1;

    tasks[1].maszyny[0]=4;
    tasks[1].maszyny[1]=9;
    tasks[1].id=2;

    tasks[2].maszyny[0]=10;
    tasks[2].maszyny[1]=8;
    tasks[2].id=3;

    tasks[3].maszyny[0]=6;
    tasks[3].maszyny[1]=4;
    tasks[3].id=4;

    tasks[4].maszyny[0]=7;
    tasks[4].maszyny[1]=2;
    tasks[4].id=5;

    tasks[5].maszyny[0]=6;
    tasks[5].maszyny[1]=3;
    tasks[5].id=6;

    cout << PrzegladZupelny(rozw,tasks);

}
